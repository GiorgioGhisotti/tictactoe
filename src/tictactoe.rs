use num_traits::cast::ToPrimitive;

pub fn cell_width (width: u16) -> u16 {
    (width.to_f32().unwrap() / 3.0)
        .to_u16()
        .unwrap()
}

pub fn cell_center (x: u8, y: u8, width: u16) -> (u16, u16) {
    let cw = cell_width(width);
    let cell_mid = (cw.to_f32().unwrap() / 2.0)
        .to_u16()
        .unwrap();
    (x as u16 * cw + cell_mid, y as u16 * cw + cell_mid)
}

pub fn grid_cell (x: u16, y: u16, width: u16) -> (u8, u8) {
    let cw = cell_width(width);
    ((x.to_f32().unwrap() / cw.to_f32().unwrap()).to_u8().unwrap(),
    (y.to_f32().unwrap() / cw.to_f32().unwrap()).to_u8().unwrap())
}

#[derive(Clone)]
pub struct Cell {
    value: String,
}

impl Cell {
    pub fn new() -> Self {
        Cell {value: String::from("")}
    }
    pub fn set(&mut self, val: &String) {
        match val.as_ref() {
            "x" if self.value == "" => self.value = String::from("x"),
            "o" if self.value == "" => self.value = String::from("o"),
            _ => return
        }
    }

    pub fn get(&self) -> &String {
        &self.value //it's a reference to avoid ownership problems
    }
}

#[derive(Clone)]
pub struct Board {
    cells: [Cell; 9],
    turn: String,
    winner: String,
}

impl Board {
    pub fn new() -> Self {
        Board {
            cells: [
               Cell::new(), Cell::new(), Cell::new(),
               Cell::new(), Cell::new(), Cell::new(),
               Cell::new(), Cell::new(), Cell::new()
            ],
            turn: String::from("x"),
            winner: String::from(""),
        }
    }

    pub fn set_cell(&mut self, x: u8, y: u8) {
        if x > 2 || y > 2 || self.winner != String::from(""){
            return
        }
        self.cells[(3 * y + x) as usize].set(&self.turn);
        self.check_winner(x, y);
        if self.cells[(3 * y + x) as usize].get() == &self.turn {
            match self.turn.as_ref() {
                "x" => self.turn = String::from("o"),
                _ => self.turn = String::from("x"),
            }
        }
    }

    pub fn get_cell(self, x: u8, y: u8) -> String {
        if x > 2 || y > 2 {
            return String::from("")
        }
        String::from(self.cells[(3 * y + x) as usize].get().as_ref())   //extract String out of reference
    }

    fn check_winner(&mut self, x: u8, y: u8) {
        if self.winner != String::from("") {
            return;
        }
        //row
        let row_index = 3 * y;
        if (self.cells[(row_index + 0) as usize].get() ==
            self.cells[(row_index + 1) as usize].get() &&
            self.cells[(row_index + 1) as usize].get() ==
            self.cells[(row_index + 2) as usize].get()) ||
        (   self.cells[(0 + x) as usize].get() ==
            self.cells[(3 + x) as usize].get() &&
            self.cells[(3 + x) as usize].get() ==
            self.cells[(6 + x) as usize].get()) {
            self.winner = String::from(self.turn.as_ref());
            return
        }
        //diagonals
        if y == x &&
                (   self.cells[(0 + 0) as usize].get() ==
                    self.cells[(3 + 1) as usize].get() &&
                    self.cells[(3 + 1) as usize].get() ==
                    self.cells[(6 + 2) as usize].get()) {
                self.winner = String::from(self.turn.as_ref());
                return
        } else if (x as i16 - y as i16).abs() == 2 &&
                (   self.cells[(0 + 2) as usize].get() ==
                    self.cells[(3 + 1) as usize].get() &&
                    self.cells[(3 + 1) as usize].get() ==
                    self.cells[(6 + 0) as usize].get()) {
            self.winner = String::from(self.turn.as_ref());
            return
        }
    }

    pub fn get_winner(&self) -> &String {
        &self.winner
    }
}

pub trait Resettable {
    fn reset(&mut self);
}

impl Resettable for Cell {
    fn reset(&mut self){
        self.value = String::from("")
    }
}

impl Resettable for Board {
    fn reset(&mut self) {
        for i in 0 .. 9 {
            self.cells[i].reset()
        }
        self.winner = String::from("");
        self.turn = String::from("x")
    }
}
