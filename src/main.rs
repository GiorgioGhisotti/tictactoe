extern crate sdl2;
extern crate num_traits;

use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::time::Duration;
use sdl2::rect::Rect;
use sdl2::gfx::primitives::*;
use num_traits::cast::ToPrimitive;

mod tictactoe;
use crate::tictactoe::*;

fn main() {
    let mut board = Board::new();

    let width: u16 = 400;

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("ttt", width as u32, width as u32)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(0,255,255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let bg_color = Color::RGB(0,0,0);
    let x_color = Color::RGB(255,64,0);
    let o_color = Color::RGB(0,255,64);
    let def_color = Color::RGB(0,0,64);
    'running: loop {
        canvas.set_draw_color(bg_color);
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(Keycode::R), .. } => {
                    board.reset();
                },
                Event::MouseButtonUp { x, y, .. } => {
                    let (i, j) = grid_cell(x as u16, y as u16, width);
                    board.set_cell(i,j);
                },
                _ => {}
            }
        }

        if board.clone().get_winner() == "x" {
            canvas.set_draw_color(x_color);
            canvas.clear();
        } else if board.clone().get_winner() == "o" {
            canvas.set_draw_color(o_color);
            canvas.clear();
        }

        for x in 0..3 {
            for y in 0..3 {
                if board.clone().get_cell(x, y) == String::from("x") {
                    canvas.set_draw_color(x_color);
                } else if board.clone().get_cell(x, y) == String::from("o") {
                    canvas.set_draw_color(o_color);
                } else if board.clone().get_cell(x, y) == String::from("") {
                    canvas.set_draw_color(def_color);
                }
                let cw = cell_width(width);
                let (cx, cy) = cell_center(x, y, width);
                canvas.fill_rect(
                    Rect::new((x as u16*cw + 2) as i32,(y as u16*cw + 2) as i32,
                                (cw - 4) as u32,(cw - 4) as u32)
                ).unwrap();
                canvas.set_draw_color(def_color);
                let half_cw: i16 = (cw as f32 / 2.0).to_i16().unwrap();
                if board.clone().get_cell(x, y) == String::from("o") {
                    canvas.filled_circle(cx as i16, cy as i16, half_cw - 12,bg_color).unwrap();
                    canvas.filled_circle(cx as i16, cy as i16, half_cw - 22,o_color).unwrap();
                } else if board.clone().get_cell(x, y) == String::from("x") {
                    canvas.thick_line(x as i16 * cw as i16 + 12,
                        y as i16 * cw as i16 + 12,
                        (x + 1) as i16 * cw as i16 - 12,
                        (y + 1) as i16 * cw as i16 - 12,
                        10,
                        bg_color).unwrap();
                    canvas.thick_line((x + 1) as i16 * cw as i16 - 12,
                        y as i16 * cw as i16 + 12,
                        x as i16 * cw as i16 + 12,
                        (y + 1) as i16 * cw as i16 - 12,
                        10,
                        bg_color).unwrap();
                }
            }
        }

        canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
